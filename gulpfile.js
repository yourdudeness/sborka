//Подклюбчение модуля галпа
const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    del = require('del'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    htmlmin = require('gulp-htmlmin'),
    cleanCSS = require('gulp-clean-css'),
    log = require('fancy-log');


//Порядок подключения css файлов

//Порядок подключения js файлов

function serve() {
    browserSync.init({
        server: {
            browser: "google chrome",
            proxy: "localhost:3001",
            baseDir: "build",
            // injectChanges: true // this is new

        }
    });
}

function clear(done) {
    return del('./build'),
        done();
}

function build_templates() {
    return gulp
        .src('./src/*.html')
        .pipe(gulp.dest('./build/'))
        .pipe(browserSync.stream());
    //  log('templates changed')
}

function img() {
    return gulp
        .src('./src/img/*.*')
        .pipe(gulp.dest('./build/img/'));
}

function watch() {
    gulp.watch('./src/css/**/*.scss', styles, browserSyncReload);
    gulp.watch('./src/js/*.js', scripts, browserSyncReload);
    gulp.watch('./src/*.html', build_templates, browserSyncReload);
    gulp.watch('./src/img/*.*', img);
}

//Таск на стили css
function styles() {
    //Шаблон для поиска файлов CSS
    //Все файлы по шаблону '.src/css/**/ *.css'

    return gulp.src('./src/css/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 2 versions'))
        //Выходная папка для стилей
        .pipe(cleanCSS({
            level: 2
        }))

        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.stream());

}
function minhtml() {
    return gulp.src('./src/*.html')
      .pipe(htmlmin({collapseWhitespace: true}))
      .pipe(gulp.dest('./build'));
  }
  

//Таск на скрипты js
function scripts() {
    return gulp.src(['./src/js/jquery.js', './src/js/index.js'])
        .pipe(uglify())
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./build/js/'))
        .pipe(browserSync.stream());
}

function browserSyncReload(done) {
    browserSync.reload();
    done();
}
//Поочередно запустить таски
const build = gulp.series(scripts, build_templates, styles, img, minhtml);

const develop = gulp.series(clear, build, gulp.parallel(watch, serve));
// Паралельно

//Таск вызывающий функцию styles
exports.styles = styles;
//Таск вызывающий функцию scripts
exports.img = img;
exports.scripts = scripts;
exports.build = build;
exports.html = minhtml;
exports.serve = serve;
exports.watch = watch;
exports.build_templates = build_templates;
exports.clear = clear;
exports.develop = develop;